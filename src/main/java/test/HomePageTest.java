package test;
/**
 * Created by Larmo on 13.01.2017.
 */
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class HomePageTest {

    public static void main(String[] args) throws Exception{

        //HashMap that contains expected links
        HashMap<String, String> expLinks = new HashMap<String, String>();

        expLinks.put("home", "https://ringlabskiev.com/");
        expLinks.put("about", "https://ringlabskiev.com/about-ring/");
        expLinks.put("careers", "https://ringlabskiev.com/careers/");
        expLinks.put("events", "https://ringlabskiev.com/events/");
        expLinks.put("contacts", "https://ringlabskiev.com/contacts/");
        expLinks.put("facebook", "https://www.facebook.com/RingLabs/");
        expLinks.put("emailUs", "mailto:info@ringlabskiev.com");

        //set path and launch chromedriver
        System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\Chrome\\chromedriver.exe");

        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        driver.get(expLinks.get("home"));

        //HashMap that contains actual links extracted by WebDriver
        HashMap<String, String> actLinks = new HashMap<String, String>();

        actLinks.put("about", driver.findElement(By.xpath(".//*[@id='topNav']/nav/ul/li[2]/a")).getAttribute("href"));
        actLinks.put("careers", driver.findElement(By.xpath(".//*[@id='topNav']/nav/ul/li[3]/a")).getAttribute("href"));
        actLinks.put("events", driver.findElement(By.xpath(".//*[@id='topNav']/nav/ul/li[4]/a")).getAttribute("href"));
        actLinks.put("contacts", driver.findElement(By.xpath(".//*[@id='topNav']/nav/ul/li[5]/a")).getAttribute("href"));
        actLinks.put("facebook", driver.findElement(By.xpath(".//*[@id='topNav']/nav/ul/li[6]/a")).getAttribute("href"));
        actLinks.put("emailUs", driver.findElement(By.xpath(".//*[@id='topNav']/nav/ul/li[7]/a")).getAttribute("href"));

        //compare actual and expected links
        for(Map.Entry<String, String> entry : actLinks.entrySet()) {
            String key = entry.getKey();
            compare(key, actLinks, expLinks);
        }

        //click "Careers" tab and check URL
        driver.findElement(By.xpath(".//*[@id='topNav']/nav/ul/li[3]/a")).click();
        if(!driver.getCurrentUrl().equals(expLinks.get("careers")))
            System.out.println("Unexpected URL: " + driver.getCurrentUrl() + "\n\t" + "Expected:" + expLinks.get("careers"));

    }

    private static void compare(String key, HashMap actLinks, HashMap expLinks){
        if(!actLinks.get(key).equals(expLinks.get(key)))
            System.out.println(key+ " Link is incorrect: " + actLinks.get(key) + "\n\t" + "Expected:" + expLinks.get(key));
    }
}
